const functions = require('firebase-functions');

const express = require('express');
const app = express();

const rateLimit = require("express-rate-limit");
const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100 // limit each IP to 100 requests per windowMs
});

const sharp = require('sharp');
const Busboy = require('busboy');
const archiver = require('archiver');

const maxFiles = 10;

function processJPEG(filestream, filename, longestEdge) {

    var resizer = sharp()
        .resize(longestEdge, longestEdge, {
            fit: sharp.fit.inside,
            withoutEnlargement: true
        })
        .jpeg({
            quality: 70,
            progressive: true,
        });

    return filestream.pipe(resizer);
}

function processWebP(filestream, filename, longestEdge) {

    var resizer = sharp()
        .resize(longestEdge, longestEdge, {
            fit: sharp.fit.inside,
            withoutEnlargement: true
        })
        .webp({
            quality: 70,
            progressive: true,
        });

    return filestream.pipe(resizer);
}

app.use('/fileUpload/', limiter);

app.post('/fileupload', (req, res) => {

    var archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
    });

    let longestEdge = 1280;
    let userLongestEdge = parseInt(req.query.longest);
    if (userLongestEdge > 0) {
        longestEdge = userLongestEdge;
    }

    var busboy = new Busboy({
        headers: req.headers,
        limits: {
            files: maxFiles
        }
    });
    busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {

        let basename = filename.split('.')[0].replace(/[^a-zA-Z]/g, "_").toLowerCase();
        let processedFileStream = processJPEG(file, filename, longestEdge);
        archive.append(processedFileStream, { name: `${basename}-${Date.now()}.jpg` });
        processedFileStream = processWebP(file, filename, longestEdge);
        archive.append(processedFileStream, { name: `${basename}-${Date.now()}.webp` });
    });
    busboy.on('finish', function () {

        archive.finalize();
        res.attachment("file.zip");
        archive.pipe(res);
    });
    busboy.end(req.body);
});

exports.api = functions.https.onRequest(app);