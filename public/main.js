
function updateSize() {
    var nBytes = 0,
        oFiles = document.getElementById("uploadInput").files,
        nFiles = oFiles.length;
    for (var nFileId = 0; nFileId < nFiles; nFileId++) {
        nBytes += oFiles[nFileId].size;
    }
    var sOutput = nBytes + " bytes";

    for (var aMultiples = ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"], nMultiple = 0, nApprox = nBytes / 1024; nApprox > 1; nApprox /= 1024, nMultiple++) {
        sOutput = nApprox.toFixed(3) + " " + aMultiples[nMultiple] + " (" + nBytes + " bytes)";
    }

    document.getElementById("fileSize").innerHTML = sOutput;
}

function saveBlob(blob, fileName) {
    var elemDownload = document.getElementById("download");
    elemDownload.href = window.URL.createObjectURL(blob);
    elemDownload.download = fileName;
    elemDownload.innerHTML = " Download";
}

function fileUpload() {
    var elemProgress = document.getElementById("progress");
    elemProgress.innerHTML = "Started";

    var oFiles = document.getElementById("uploadInput").files;
    var elemLongest = document.getElementById("longest");
    var longest = elemLongest.options[elemLongest.selectedIndex].value;

    var uri = `/fileupload?longest=${longest}`;
    var xhr = new XMLHttpRequest();
    var fd = new FormData();

    xhr.open("POST", uri, true);

    for (var i = 0; i < oFiles.length; i++) {
        fd.append('file', oFiles[i]);
    }

    xhr.responseType = "blob";
    xhr.onload = function (e) {
        elemProgress.innerHTML = "Finished";
        var blob = xhr.response;
        var fileName = xhr.getResponseHeader("Content-Disposition").match(/\sfilename="([^"]+)"(\s|$)/)[1];
        saveBlob(blob, fileName);
    }

    xhr.send(fd);
}

function fileEncode() {

    var oFiles = document.getElementById("uploadInput").files;
    var elemResult = document.getElementById("encodeResult");
    var elemEncoded = document.getElementById("encoded");
    var elemLineBreak = document.getElementById("lineBreakInput");

    if (FileReader && oFiles && oFiles.length) {
        var fr = new FileReader();
        fr.onload = function () {
            let result = fr.result;
            if (elemLineBreak && elemLineBreak.checked) {
                const regex = /[a-zA-Z0-9/=+ ]{100}/g;
                result = fr.result.replace(regex, function (match) { return match + '\\\n'; })
            }
            elemResult.src = result;
            elemEncoded.value = result;
        }
        fr.readAsDataURL(oFiles[0]);
    }
}
